var App = App || {};

(function(angular, app){
	app.fbModule = angular.module('facebook', ['ngRoute']);

	app.fbModule.config(['$routeProvider', '$provide', function($routeProvider, $provide) {
		/* Add New Routes Above */
		$routeProvider.when('/fb', {templateUrl: 'facebook/templates/facebook.html'});
	}]);

	app.fbModule.run(["$rootScope", "fbService", function ($rootScope, fbService) {
		fbService.initialize($rootScope);
	}]);
})(angular, App);

