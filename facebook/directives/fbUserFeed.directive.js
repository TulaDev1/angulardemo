var App = App || {};
(function(app){
    app.fbModule.directive('fbUserFeed', function () {
        return {
            restrict: 'E',
            templateUrl: 'facebook/templates/facebook-feed.html',
            controller: 'facebookFeedController',
            controllerAs: 'fbFeedCtrl',
            replace: true
        };
    });
})(App);

