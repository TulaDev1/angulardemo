var App = App || {};
(function(app){
    app.fbModule.directive('fbFeedItem', function () {
        return {
            restrict: 'A',
            templateUrl: 'facebook/templates/facebook-feed-item.html',
            controller: 'facebookFeedItemController',
            controllerAs: 'fbFeedItemCtrl'
        };
    });
})(App);

