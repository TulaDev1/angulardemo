var App = App || {};

(function(app){
    app.fbModule.controller('facebookFeedItemController', [ function(){
        var store = this;

        store.getBadgeClass = function(type) {
             var css = {};
            switch (type)
            {
                case 'photo': { css.success = true; } break;
                case 'link': { css.info = true; } break;
                case 'video': { css.warning = true; } break;
                case 'status': { css.danger = true; } break;
            }

            return css;
        };

        store.getIconClass = function(type) {
            var css = {};
            switch (type)
            {
                case 'created_note':
                case 'tagged_in_photo': {
                    css['glyphicon-credit-card'] = true;
                } break;
                case 'wall_post': {
                    css['glyphicon-floppy-disk']= true;
                } break;
                case 'shared_story': {
                    css['glyphicon-check'] = true;
                } break;
                case 'added_photos': {
                    css['glyphicon-check'] = true;
                } break;
                case 'mobile_status_update': { css['glyphicon-time'] = true; } break;
                default :{css['glyphicon-thumbs-up'] = true;}
            }
            return css;
        };

        store.getColumnClass = function(item) {
            var css = {},
                hasDescription = store.hasDescription(item),
                hasImage = store.hasImage(item);

            if(hasDescription && hasImage){
                css['col-xs-6'] = true;
            } else {
                css['col-xs-12'] = true;
            }
            return css;
        };

        store.hasDescription = function(item) {
            return !!(item && (item.message || item.description));
        };

        store.hasImage = function(item) {
            return !!(item && item.picture);
        };
    }]);
})(App);
