var App = App || {};

(function(app){
    app.fbModule.controller('facebookFeedController', [ '$scope', 'fbService', function($scope, fbService){
        var store = this,
            __updateUserData = function (data) {
                store.userData = data;
            },
            __dispose = function(){
                window.localStorage.fb_token = '';
                store.isLogged = false;
                store.userData = {};
                store.feedItems = [];
            };

        //throw 'exception';
        store.isLogged = false;
        store.isFbReady = false;
        store.userData = {};
        store.feedItems = [];

        fbService.getLoginStatus().then(function (response) {
            store.isFbReady = true;
            store.isLogged = response.status === 'connected';

            if(store.isLogged) {
                store.getFeed();
            }
        }, function (response) {
            console.log('getLoginStatus promise failed');
        });

        store.login = function (){
            fbService.login();
        };

        $scope.$on("fbStatus", function (scope, value) {
                store.isLogged = value;
                if(store.isLogged) {
                    store.getFeed();
                }
                else{
                    __dispose();
                }
                if(!$scope.$$phase) {
                    $scope.$apply();
                }
        });

        store.logout = function(){
            fbService.logout().then(function (response) {
                __dispose();
            }, function (response) {
                console.log('logout promise failed');
            });
        };

        store.getFeed = function(){
            fbService.getUserData().then(function(response){
                __updateUserData(response);
            }, function(response){
                console.log('getUserData promise failed');
            });

            fbService.getFeed().then(function (response) {
                store.feedItems = response.data;
            }, function (response) {
                console.log('getFeed promise failed');
            });
        };

        store.setTimeLineClass = function(index){
            return index % 2 === 0;
        };
    }]);
})(App);
