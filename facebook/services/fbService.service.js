
/* global FB: true,
OAuth: true,*/

var App = App || {};

(function(app){
	app.fbModule.factory('fbService', ['$q', function ($q) {
		var $rootScope= null , facebookLogged = false;

		function updateStatus(status){
			facebookLogged= status;
			$rootScope.$broadcast("fbStatus", facebookLogged);
		}

		var _login = function () {
			var deferred = $q.defer();

			FB.login(function(response) {
				deferred.resolve(response);
			}, {
				scope: 'read_stream'
			});

			return deferred.promise;
		};

		var _getLoginStatus = function(){
			var deferred = $q.defer();

			FB.getLoginStatus(function(response) {
				deferred.resolve(response);
			});

			return deferred.promise;
		};

		var _getUserData = function(){
			var deferred = $q.defer();

			FB.api('/me', function(response) {
				deferred.resolve(response);
			});

			return deferred.promise;
		};

		var _logout = function(){
			var deferred = $q.defer();

			FB.logout(function(response) {
				deferred.resolve(response);
			});

			return deferred.promise;
		};

		var _getFeed = function(){
			var deferred = $q.defer();

			FB.api('/me/feed',function(response) {
				deferred.resolve(response);
			});

			return deferred.promise;
		};

		window.onFbInit = function () {
			FB.Event.subscribe('auth.statusChange', function(loginResponse) {
				updateStatus(loginResponse.status==="connected");
			});
			
			/*FB.Event.subscribe('auth.login', function(loginResponse) {
				if (loginResponse.authResponse) {
					  window.localStorage.fb_token = loginResponse.authResponse.accessToken;
					  updateStatus(true);
				} else {
					//processing an error
				}});

			FB.Event.subscribe('auth.logout', function(logoutResponse) {
				updateStatus(false);
			});*/
			_getLoginStatus().then(function (response){
				 updateStatus(response.status === "connected");
	   });};


		return {
			login: _login,
			getLoginStatus: _getLoginStatus,
			getUserData: _getUserData,
			logout: _logout,
			getFeed: _getFeed,
			isLoggedIn :function() {return facebookLogged;},
			initialize: function(rootScope){
				$rootScope = rootScope;
				 if (!!window.FB) {
					window.onFbInit();
					window.onFbInit = null;
				  }
			}
		};
	}]);
})(App);
