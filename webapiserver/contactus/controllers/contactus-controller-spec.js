describe('ContactusCtrl', function() {

    beforeEach(function(){
        module('webapiserver');
    });

	var ctrl,
        $httpBackend,
        postContactUsUrl = 'http://localhost:59138/api/support/contactus',
        promise,
        successCallback,
        errorCallback,
        formValidationService;

    beforeEach(inject(function($controller, _$httpBackend_) {
      $httpBackend = _$httpBackend_;
      formValidationService = jasmine.createSpyObj('formValidationService', ['setFormErrors']);
      ctrl = $controller('ContactUsCtrl', {$http: $httpBackend, formValidationService: formValidationService});
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

	it('should have Contact Us controller', inject(function() {
        expect(ctrl).toBeDefined();
	}));

});