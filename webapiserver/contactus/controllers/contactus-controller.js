(function () {
    "use strict";

    angular.module('webapiserver').controller("ContactUsCtrl", ['$http', 'formValidation', function ($http, formValidationService) {
        var vm = this;
        vm.contactUsModel = {
            name: '',
            email: '',
            message: ''
        };

        vm.invalid = false;
        vm.isSent = false;
        vm.contactusModal = null;

        vm.submitMessage = function(){
            $http.post("http://localhost:59138/api/support/contactus", vm.contactUsModel)
                .success(function() {
                    vm.isSent = true;
                }).error(function(data) {
                    if(data && data.length){
                        formValidationService.setFormErrors({
                            formName: 'contactForm',
                            modelState: data.ModelState
                        });
                    }
                });
        }
    }]);
}());