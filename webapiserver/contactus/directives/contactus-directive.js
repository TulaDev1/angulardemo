angular.module('webapiserver').directive('contactusDirective', function() {
	return {
		restrict: 'E',
		replace: true,
        controller: 'ContactUsCtrl',
        controllerAs: 'cusCtrl',
		templateUrl: 'webapiserver/contactus/directives/contactus-directive.html',
		link: function(scope, element, attrs, fn) {
		}
	};
});
