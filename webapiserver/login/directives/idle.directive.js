var App = App || {};

(function(app){
    app.webApiServer.directive('idleNotification', function () {
        return {
            restrict: 'E',
            controller: 'idleModalController',
            controllerAs: 'idleModalCtrl'
        };
    });
})(App);

