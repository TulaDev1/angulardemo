var App = App || {};

(function(app){

    app.webApiServer.config(['idleServiceProvider', function(idleServiceProvider){
        idleServiceProvider.settings({
            sessionDuration: 15,
            expirationReminder: 10
        });
    }]);

})(App);

