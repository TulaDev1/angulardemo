var App = App || {};

(function(app){
    app.webApiServer.controller('idleModalController', ['$modal', '$rootScope', '$scope', function($modal, $rootScope, $scope){
        var self = this,
            modal = null;

        //SessionExpireEvent
        $rootScope.$on("SessionExpireReminderEvent", function (scope, value) {
            $scope.remainingTime = value;

            modal = $modal({
                scope: $scope,
                contentTemplate: 'webapiserver/login/templates/idle-notification.html',
                show: true
            });
        });

        $scope.$on('idleController_ResumeEvent', function(scope, value){
            console.log('idleController_ResumeEvent');

            if(modal)
                modal.destroy();
        });

    }]);

    app.webApiServer.controller('idleController', ['$interval', '$scope', 'idleService', function($interval, $scope, idleService){
        var self = this,
            intervalId;

        self.remainingTime = $scope.remainingTime;
        self.isExpired = false;

        self.resume = function(){
            idleService.restartSession();
            $scope.$emit('idleController_ResumeEvent');
        };
        self.cancel = function(){
            $scope.$emit('idleController_CancelEvent');
        };
        self.login = function(){
            $scope.$emit('idleController_LoginEvent');
        };

        intervalId = $interval(function () {
            self.remainingTime -= 1;

            if (!self.remainingTime) {
                $interval.cancel(intervalId);
                intervalId = null;

                self.isExpired = true;
            }
        }, 1000);
    }]);
})(App);
