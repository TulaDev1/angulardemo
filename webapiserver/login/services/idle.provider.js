var App = App || {};

(function(app){
    app.webApiServer.provider('idleService', [ function () {
        var _options = {
                sessionDuration: 15,
                expirationReminder: 5
            };

        return {
            settings: function(options){
                angular.extend(_options, options)
            },

            $get: ['$interval', '$rootScope', function($interval, $rootScope){
                var  intervalId = null,
                    _startSessionCountdown = function(){
                        var duration = _options.sessionDuration,
                            reminder = _options.expirationReminder;

                        intervalId = $interval(function () {
                            duration -= 1;

                            if (duration == reminder) {
                                $rootScope.$broadcast('SessionExpireReminderEvent', reminder);
                            }

                            if (!duration) {
                                _stopSessionCountdown();

                                $rootScope.$broadcast('SessionExpireEvent');
                            }
                        }, 1000);
                    },
                    _stopSessionCountdown = function(){
                        if(intervalId)
                            $interval.cancel(intervalId);

                        intervalId = null;
                    },
                    _restartSessionCountdown = function(){
                        _stopSessionCountdown();
                        _startSessionCountdown();
                    };

                return {
                    startSession: _startSessionCountdown,
                    restartSession: _restartSessionCountdown,
                    stopSession: _stopSessionCountdown
                };
            }]
        };
    }]);
})(App);
