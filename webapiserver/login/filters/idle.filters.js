var App = App || {};

(function(app){

    app.webApiServer.filter('secondsToTime', function() {
        return function(input) {
            var secNum = parseInt(input),
                hours = Math.floor(secNum / 3600),
                minutes = Math.floor((secNum - (hours * 3600)) / 60),
                seconds = secNum - (hours * 3600) - (minutes * 60),
                time = '';

            if (hours) {
                time = (hours < 10 ? '0' + hours : hours) + ':';
            }

            time += (minutes < 10 ? '0' + minutes : minutes) + ':';
            time += (seconds < 10 ? '0' + seconds : seconds);

            return time;
        };
    });

})(App);
