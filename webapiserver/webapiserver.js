
var App = App || {};

App.webApiServer = angular.module('webapiserver', [ 'ui.bootstrap', 'ui.utils', 'ngRoute', 'ngAnimate', 'common', 'mgcrea.ngStrap.modal' ]);
App.webApiServer.config(['$httpProvider', '$routeProvider', function($httpProvider, $routeProvider, idleServiceProvider) {
    $httpProvider.interceptors.push(['$rootScope','$q',function($rootScope, $q) {
        return {
            'responseError' : function(rejection) {
                var defer = $q.defer();
                if (rejection.status == 401) {
                    if (!!window.localStorage.authToken) {
                        window.localStorage
                                .clear();
                        $rootScope.$broadcast("token_expired", true);
                    }
                }
                defer.reject(rejection);
                return defer.promise;
            }
        };
    }]);
}])
		.value('WebApiServerUrl', 'http://localhost:59138/')
		.factory(
				'AngToken',
				[
						"$resource",
						'WebApiServerUrl',
						function($resource, url) {
							return $resource(
									url + 'token',
									{},
									{
										login : {
											method : "POST",
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
											},
											crossDomain : true,
										},
									});

						} ]).factory(
				'AngData',
				[
						"$resource",
						'WebApiServerUrl',
						function($res, url) {
							return $res(url + '/api/SessionData', {}, {
								query : {
									method : 'GET',
									headers : {
										"Authorization" : "Bearer "
												+ window.localStorage.authToken
									},
									error : function() {
										console.info(arguments);
									}
								},
								update : {
									method : "POST"
								}
							});

						} ]);

