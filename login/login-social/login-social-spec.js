describe('LoginSocialCtrl', function() {

    beforeEach(module('login'));

    var scope, ctrl;

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        ctrl = $controller('LoginSocialCtrl', {$scope: scope});
    }));
    it('login controller should be defined', inject(function() {
        expect(ctrl).toBeDefined();
    }));
    it('facebook login function should be defined', inject(function() {
        expect(scope.facebookToggle).toBeDefined();
    }));
    it('facebook should be logged off', inject(function() {
        expect(ctrl.isFacebookLoggedIn()).toEqual(false);
    }));
    it('twitter login function should be defined', inject(function() {
        expect(scope.twitterToggle).toBeDefined();
    }));
    it('twitter should be logged off', inject(function() {
        expect(ctrl.isTwitterLoggedIn()).toEqual(false);
    }));

});