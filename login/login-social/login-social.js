angular.module('login').controller('LoginSocialCtrl', ["$scope", "$location", "twitterService","fbService", function($scope, $location,twitterService,fbService) {
		var self = this;


		/// Facebook


		self.facebookLogged= fbService.isLoggedIn();

		this.isFacebookLoggedIn = function(){
			return self.facebookLogged;
		};

		self.facebookToggle = function(){
			if (fbService.isLoggedIn()) {
				 fbService.logout();
			} else {
				fbService.login();
			}
		 };

		$scope.$on("fbStatus", function (scope, value) {
			 self.facebookLogged= value;
			 if(!$scope.$$phase) {
				 $scope.$apply();
				}

		});

	 //* Twitter*//
		 self.twitterLogged = !!twitterService.isReady();
		 this.isTwitterLoggedIn = function(){
			 return self.twitterLogged;
		 };



		self.twitterToggle = function(){
			if (twitterService.isReady()) {
				twitterService.clearCache();
			} else {
				twitterService.connectTwitter();
			}
		};
		$scope.$on("twitterStatus", function (scope, value) {
		 self.twitterLogged= value;
		if(!$scope.$$phase) {
		 $scope.$apply();
		}
		});

	}]);

