angular.module('login').directive('loginSocial', function() {
	return {
		restrict: 'E',
		replace :true,
		controller: 'LoginSocialCtrl',
		controllerAs: 'loginCtrl',
		templateUrl: '/login/login-social/login-social.html'		
	};
});