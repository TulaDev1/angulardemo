angular.module('login', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate']);

angular.module('login').config(['$routeProvider',function($routeProvider) {
   $routeProvider.when('/login',{controller:'LoginSocialCtrl', templateUrl: '/login/login-social/login-social.html'});
}]);

