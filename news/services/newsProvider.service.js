var App = App || {};

(function (app) {
	"use strict";

	app.news.service("newsProvider", ["$http", function ($http) {
		var service = this;
		this.newsReady = null;
		this.endpoint = "https://qqilihq-newsseecr.p.mashape.com/news?coordinates=48.1%2C11.5&distance=10&query=Obama";

		this.loadNews = function () {
			var config = {
				headers: {
					"X-Mashape-Key": "r6ZUfMbEurmsh7BmOp5tmhPCu7UXp1m9ZbzjsnrAxLnzzuggx2"
				}
			};

			// TODO: paging
			// TODO: error handling
			// TODO: Event management (for subscription)
			// TODO: data mapping fixes
			// TODO: news details

			$http
				.get(this.endpoint, config)
				.success(function (data) {
					var newsData = $.map(data.results, function (record) {
						return {
							title: record.title,
							date: record.published,
							link: record.url,
							text: record.summary
						};
					}).slice(5, 10);

					if (service.newsReady) {
						service.newsReady(newsData);
					}
				});
		};
	}]);
}(App));