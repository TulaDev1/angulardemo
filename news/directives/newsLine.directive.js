var App = App || {};

(function (app) {
	"use strict";

	app.news.directive("newsLine", function () {
		return {
			restrict: "E",
			replace: true,
			controller: "NewsLineCtrl",
			controllerAs: "newsLineCtrl",
			templateUrl: "/news/templates/newsLine.html"
		};
	});
}(App));