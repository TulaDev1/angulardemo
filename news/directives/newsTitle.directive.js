var App = App || {};

(function (app) {
	"use strict";

	app.news.directive("newsTitle", function () {
		return {
			restrict: "A",
			scope: {
				title: "=newsTitle"
			},
			link: function (scope, element) {
				element.text(scope.title);

				var dom = element.get(0);
				var overflows = dom.clientWidth < dom.scrollWidth;

				if (overflows) {
					element.tooltip({
						placement: "right",
						container: "body",
						title: scope.title
					});
				}
			}
		};
	});
}(App));