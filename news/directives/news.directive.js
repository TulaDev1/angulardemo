angular.module('news').directive('newsDirective', function() {
	return {
		restrict: 'E',
		controller: 'NewsCtrl',
		controllerAs: 'NewsCtrl',
		templateUrl: '/news/templates/news.partial.html',
		replace: true
	};
});
