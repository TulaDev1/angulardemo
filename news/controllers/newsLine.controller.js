var App = App || {};

(function (app) {
	"use strict";

	app.news.controller("NewsLineCtrl", ["newsProvider", function (newsProvider) {
		var controller = this;
		this.busy = false;
		this.configModal = null;

		this.showConfig = function () {
			this.configModal.entity.endpoint = newsProvider.endpoint;
			this.configModal.showModal();
		};

		this.refresh = function () {
			newsProvider.loadNews();
			this.busy = true;
		};

		this.controlsEnabled = function () {
			return this.configModal != null  ? !this.configModal.visible : true;
		};

		newsProvider.newsReady = function (newsData) {
			controller.news = newsData;
			controller.busy = false;
		};

		this.refresh();
	}]);
}(App));