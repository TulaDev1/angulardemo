var App = App || {};

(function (app) {
	"use strict";

	app.news = angular.module("news", ["ui.bootstrap", "ui.utils", "ngRoute", "ngAnimate"]);

	app.news.config(["$routeProvider", function ($routeProvider) {
		$routeProvider.when("/news", { controller: "NewsCtrl", templateUrl: "/news/templates/news.html" });
	}]);
}(App));