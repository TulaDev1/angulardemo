var App = App || {};

(function (app) {
    'use strict';

    //Configure toastr
    app.tulaangulardemo.config(['toastrConfig', function (toastrConfig){
        angular.extend(toastrConfig, {
            allowHtml: true,
            closeButton: true,
            closeHtml: '<button>&times;</button>',
            containerId: 'toast-container',
            extendedTimeOut: 1000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            messageClass: 'toast-message',
            positionClass: 'toast-top-right',
            tapToDismiss: true,
            timeOut: 3000,
            titleClass: 'toast-title',
            toastClass: 'toast'
        });
    }]);

    //Configure the exceptionHandler decorator
    var config = {
        appErrorPrefix: 'app says: '
    };

    app.tulaangulardemo.value('config', config);

    app.tulaangulardemo.config(configure);

    function configure ($logProvider, exceptionConfigProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }

        // Configure the common exception handler
        exceptionConfigProvider.config.appErrorPrefix = config.appErrorPrefix;
    }
})(App);