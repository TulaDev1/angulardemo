var App = App || {};

(function(angular, app){

    app.about = angular.module('about', ['ngRoute']);

    app.about.config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/about', {templateUrl: '/about/templates/about.html'});
    }]);

})(angular, App);
