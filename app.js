var App = App || {};

(function(angular, app){
	'use strict';

	app.tulaangulardemo = angular.module('tulaangulardemo', ['ui.bootstrap', 'ui.utils', 'ngRoute', 'ngAnimate', 'common', 'login', 'news', 'facebook', 'twitter', 'home', 'about', 'webapiserver']);
	app.tulaangulardemo.config(["$routeProvider", function($routeProvider) {
        /* Add New Routes Above */
        $routeProvider.otherwise({redirectTo:'/'});
    }]);

	app.tulaangulardemo.run(["$rootScope", function($rootScope) {

	$rootScope.safeApply = function(fn) {
		var phase = $rootScope.$$phase;
		if (phase === '$apply' || phase === '$digest') {
			if (fn && (typeof(fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};
 }]);
})(angular, App);

