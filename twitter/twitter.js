angular.module('twitter', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate', 'common']);

angular.module('twitter').config(['$routeProvider', function($routeProvider) {
    $routeProvider.when("/twitter", {templateUrl: '/twitter/twitter-timeline/partials/twitter-timeline-partial.html'});
}]);

angular.module('twitter').run(["$rootScope", "twitterService", function ($rootScope, twitterService) {
	twitterService.initialize($rootScope);
}]);
