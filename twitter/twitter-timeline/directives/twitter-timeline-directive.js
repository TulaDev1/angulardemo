angular.module('twitter').directive('twitterTimeleneDirective', function() {
	return {
		restrict: 'E',
		controller: 'TwitterTimelineCtrl',
		controllerAs: 'ttCtrl',
		templateUrl: '/twitter/twitter-timeline/directives/twitter-timeline-directive.html',
		replace: true
	};
});
