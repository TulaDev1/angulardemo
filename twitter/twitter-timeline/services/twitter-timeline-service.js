/* global FB: true,
OAuth: true,*/
angular.module('twitter').service('twitterService', [ '$q', function ($q) {
	var $rootScope = null, authorizationResult = false;

	function updateStatus(status){
		authorizationResult= status;
		$rootScope.$broadcast("twitterStatus", authorizationResult);
	}

	return {
		initialize: function(rootScope) {
			$rootScope = rootScope;
			OAuth.initialize(window.socials.twitter, {cache:true});
			updateStatus(OAuth.create('twitter'));
		},

		isReady: function() {
			return (authorizationResult);
		},

		connectTwitter: function() {
			var deferred = $q.defer();
			OAuth.popup('twitter', {cache:true}, function(error, result) {
				if (!error) {
					updateStatus(result);
					deferred.resolve();
				} else {
				}
			});
			return deferred.promise;
		},

		clearCache: function() {
			OAuth.clearCache('twitter');
			updateStatus(false);
		},

		getUserTimeLine: function () {
			var deferred = $q.defer();
			$rootScope.$broadcast("loader_show");
			var promise = authorizationResult.get('/1.1/statuses/user_timeline.json').done(function(data) {
				$rootScope.$broadcast("loader_hide");
				deferred.resolve(data);
			});
			return deferred.promise;
		},

		postToTimeLine: function (text) {
			var deferred = $q.defer();
			$rootScope.$broadcast("loader_show");
			var promise = authorizationResult.post({
				url: '/1.1/statuses/update.json',
				data: {
					status: text
				}
			}).done(function(data) {
				$rootScope.$broadcast("loader_hide");
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
}]);
