angular.module('twitter').controller('TwitterTimelineCtrl',['twitterService', '$scope', function(twitterService, $scope){
	var vm = this;
	vm.timelines = [];
    vm.tweet = '';
    vm.userName = '';
	vm.isLogged = twitterService.isReady();
    vm.isLoading = true;

    vm.setTimeLineClass = function(index){
		return index % 2 === 0;
	};

    vm.login = function(){
        twitterService.connectTwitter().then(vm.loadTimeLine);
    };

    vm.loadTimeLine = function(){
        vm.isLoading = true;
        twitterService.getUserTimeLine().then(function(feeds){
            vm.timelines = feeds;
            vm.isLoading = false;
        },
        function(feeds){});
    };

    vm.setDisabled = function(){
        return !vm.tweet || vm.tweet.length === 0;
    };

    vm.postToTimeLine = function() {
        vm.isLoading = true;
        twitterService.postToTimeLine(vm.tweet).then(function(){
            vm.loadTimeLine();
            vm.tweet = '';
        } );
    };

    vm.hasImage = function(item) {
        return !!(item && item.entities && item.entities.media && item.entities.media[0] && item.entities.media[0].media_url);
    };

    if(vm.isLogged) {
        vm.loadTimeLine();
    }

    $scope.$on("twitterStatus", function () {
        vm.isLogged = twitterService.isReady();
        if(vm.isLogged) {
            vm.loadTimeLine();
        }
    });
}]);
