angular.module('home', ['ui.bootstrap','ui.utils','ngRoute','ngAnimate']);

angular.module('home').config(function($routeProvider) {

    $routeProvider.when('/',{templateUrl: 'home/home.html', controller : 'HomeController', controllerAs : 'HomeCtrl'});
    /* Add New Routes Above */

});

