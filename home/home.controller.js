angular.module('home').controller('HomeController', ['$scope', 'idleService',  function($scope, idleService) {
	var features = [ "start","fb-home", "fb","twitter-home", "twitter","news-home","news", "about" ]
	var current = 0;
	var self = this;
	self.prevFeatureHref = true;
	self.nextFeatureHref = true;
	function checkButtons() {
		self.prevFeatureHref = current > 0;
		self.nextFeatureHref = current < features.length-1;
	}
	this.next = function() {
		current++;
		if (current>=features.length) current = features.length-1;
		checkButtons();
	}
	
	this.prev = function() {
		current--;
		if (current<0) current = 0;
		checkButtons();
	}
	checkButtons();

	this.isSelected = function(name){
		return features[current]==name ? 'active' : '';
	}
	
	this.setCurrent = function (value){
		if (features.indexOf(value) >-1) current = features.indexOf(value);
		else current = value; 
		if (current<0) current = 0;
		if (current>=features.length) current = features.length-1;
		checkButtons();
	}

    //TODO: Remove to login service/controller
    idleService.startSession();
}]);