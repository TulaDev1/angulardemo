var App = App || {};

(function (app) {
	"use strict";

	app.common.controller("FeatureTitleCtrl", ["$scope", function ($scope) {
		this.title = $scope.featureText;
		this.subtitle = $scope.featureSubText;
		this.prevFeatureHref = !!$scope.prevRoute?"#" + $scope.prevRoute:false;
		this.nextFeatureHref = !!$scope.nextRoute?"#" + $scope.nextRoute:false;
	}]);
}(App));