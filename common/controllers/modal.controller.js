var App = App || {};

(function (app) {
	"use strict";

	app.common.controller("ModalCtrl", ["$scope", function ($scope) {
		var controller = this;
		this.elementId = $scope.modalId;
		this.title = $scope.modalTitle;
		this.visible = false;
		this.entity = {};

		$scope.selfReference = this;

		this.showModal = function () {
			$("#" + this.elementId)
				.modal("show")
				.on("hidden.bs.modal", function () {
					controller.visible = false;
					$scope.$apply();
				});

			this.visible = true;
		};
	}]);
}(App));