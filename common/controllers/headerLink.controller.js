var App = App || {};

(function (app) {
	"use strict";

	app.common.controller("HeaderLinkCtrl", ["$scope", "navigator", function ($scope, navigator) {
		var controller = this;
		this.title = $scope.navText;
		this.route = $scope.navRoute;
		this.href = "#" + $scope.navRoute;
		this.active = false;

		navigator.trackScope(this.route, $scope, function (active) {
			controller.active = active;
		});
	}]);
}(App));