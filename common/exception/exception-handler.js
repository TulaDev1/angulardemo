var App = App || {};

(function (app) {
    'use strict';

    app.common
        .provider('exceptionConfig', exceptionConfigProvider)
        .config(exceptionConfig);

    function exceptionConfigProvider() {
        /* jshint validthis:true */
        this.config = {
            // These are the properties we need to set
            //appErrorPrefix: ''
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    }

    exceptionConfig.$inject = ['$provide'];

    function exceptionConfig($provide) {
        $provide.decorator('$exceptionHandler', extendExceptionHandler);
    }

    extendExceptionHandler.$inject = ['$delegate', 'exceptionConfig', '$injector'];

    function extendExceptionHandler($delegate, exceptionConfig, $injector) {
        var appErrorPrefix = exceptionConfig.config.appErrorPrefix || '';
        return function (exception, cause) {
            $delegate(exception, cause);
            var errorData = { exception: exception, cause: cause };
            var exceptionMessage = exception.message || 'unexpected error occurred please try again later';
            var msg = appErrorPrefix + exceptionMessage;
            /**
             * Could add the error to a service's collection,
             * add errors to $rootScope, log errors to remote web server,
             * or log locally. Or throw hard. It is entirely up to you.
             * throw exception;
             *
             * @example
             *     throw { message: 'error message we added' };
             *
             */
            var logger = $injector.get('logger');
            logger.logError(msg, errorData, "common" ,true);
        };
    }
})(App);