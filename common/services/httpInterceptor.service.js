var App = App || {};

(function (app) {
	"use strict";

	app.common.service("httpInterceptor", ['$q', '$rootScope', function ($q, $rootScope) {
        var numLoadings = 0;

        return {
            request: function (config) {
                numLoadings++;
                // Show loader
                $rootScope.$broadcast("loader_show");
                return config || $q.when(config);
            },
            response: function (response) {
                if ((--numLoadings) === 0) {
                    // Hide loader
                    $rootScope.$broadcast("loader_hide");
                }
                return response || $q.when(response);
            },
            responseError: function (response) {
                if (!(--numLoadings)) {
                    // Hide loader
                    $rootScope.$broadcast("loader_hide");
                }
                //When an API is offline
                if(response.status == 0) {
                    throw { message: "Connection problems" };
                }
                return $q.reject(response);
            }
        };
	}]).config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
    }]);
}(App));