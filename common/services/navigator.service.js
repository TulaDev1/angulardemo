var App = App || {};

(function (app) {
	"use strict";

	app.common.service("navigator", [function () {
		var service = this;
		this.rootScope = null;
		this.currentRoute = null;

		this.bindScope = function ($rootScope) {
			this.rootScope = $rootScope;
			this.rootScope.$on("$routeChangeSuccess", function (event, info) {
				service.routed(info.$$route);
			});
		};

		this.trackScope = function (url, $scope, callback) {
			callback(this.check(url));

			$scope.$on("routed", function () {
				callback(service.check(url));
			});
		};

		this.routed = function (route) {
			if (route.redirectTo === undefined) {
				this.currentRoute = route;
				this.rootScope.$broadcast("routed", route);
			}
		};

		this.check = function (url) {
			return this.currentRoute && this.currentRoute.originalPath === url;
		};
	}]);
}(App));