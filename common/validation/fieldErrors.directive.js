var App = App || {};

(function (app) {
    "use strict";

    app.common.directive("fieldErrors", function () {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            require: ['fieldErrors', '^withErrors'],
            template: '<div ng-repeat="error in errors"><small class="error">{{ error }}</small></div>',
            controller: ['$scope', function($scope) {
                $scope.errors = [];
                this.setErrors = function(errors) {
                    $scope.errors = errors;
                };
                this.clearErrors = function() {
                    $scope.errors = [];
                };
            }],
            link: function(scope, element, attrs, ctrls) {
                var fieldErrorsCtrl = ctrls[0];
                var withErrorsCtrl = ctrls[1];
                withErrorsCtrl.addControl(attrs.for, fieldErrorsCtrl);
            }
        };
    });
}(App));