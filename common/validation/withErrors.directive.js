var App = App || {};

(function (app) {
    "use strict";

    app.common.directive("withErrors", ['formValidation', function (formValidationService) {
        return {
            restrict: 'A',
            require: 'withErrors',
            controller: ['$scope', '$element', function($scope, $element) {
                var controls = {};

                this.addControl = function(fieldName, ctrl) {
                    controls[fieldName] = ctrl;
                };

                this.setErrorsFor = function(fieldName, errors) {
                    if (!(fieldName in controls)) return;
                    return controls[fieldName].setErrors(errors);
                };

                this.clearErrorsFor = function(fieldName, errors) {
                    if (!(fieldName in controls)) return;
                    return controls[fieldName].clearErrors(errors);
                };
            }],
            link: function(scope, element, attrs, ctrl) {
                // Make this form controller accessible to formValidation service
                formValidationService._register(attrs.name, ctrl);
            }
        };
    }]);
}(App));