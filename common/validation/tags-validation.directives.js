var App = App || {};

(function (app) {
    "use strict";

    app.common.directive("input", function () {
        return {
            restrict: 'E',
            require: ['?ngModel', '?^withErrors'],
            scope: true,
            link: link
        }
    });

    app.common.directive("textarea", function () {
        return {
            restrict: 'E',
            require: ['?ngModel', '?^withErrors'],
            scope: true,
            link: link
        }
    });

    function link (scope, element, attrs, ctrls) {
        var ngModelCtrl = ctrls[0];
        var withErrorsCtrl = ctrls[1];
        var fieldName = attrs.name;

        if (!ngModelCtrl || !withErrorsCtrl) return;

        // Watch for model changes and set errors if any
        scope.$watch(attrs.ngModel, function() {
            if (ngModelCtrl.$dirty && ngModelCtrl.$invalid) {
                withErrorsCtrl.setErrorsFor(fieldName, errorMessagesFor(ngModelCtrl));
            } else if (ngModelCtrl.$valid) {
                withErrorsCtrl.clearErrorsFor(fieldName);
            }
        });

        // Mapping Angular validation errors to a message
        var errorMessages = {
            required: 'This field is required',
            pattern: 'This field does not match pattern',
            minlength: 'This field is too long',
            maxlength: 'This field is too short',
            email: 'The Email field is not a valid e-mail address'
            // etc.
        };

        function errorMessagesFor(ngModelCtrl) {
            return Object.keys(ngModelCtrl.$error).
                map(function(key) {
                    if (ngModelCtrl.$error[key]) return errorMessages[key];
                    else return null;
                }).
                filter(function(msg) {
                    return msg !== null;
                });
        }
    };
}(App));