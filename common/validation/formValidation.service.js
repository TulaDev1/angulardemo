var App = App || {};

(function (app) {
    'use strict';
    app.common.service('formValidation', formValidationService);

    function formValidationService(){
        // Registered withErrors controllers
        var withErrorCtrls = {};

        // The exposed service
        var service = {
                setFormErrors: setFormErrors,
                parseModelState: parseModelState,
                _register: _register
            };

        return service;

        function setFormErrors(opts){
            var fieldErrors =  parseModelState(opts.modelState);
            var ctrl = withErrorCtrls[opts.formName];

            angular.forEach(fieldErrors, function(value) {
            ctrl.setErrorsFor(value.fieldName, value.errorMessage);
            });
        }

        function parseModelState(modelState){
            var errors = [];
            for (var fieldName in modelState) {
                var error = {fieldName: fieldName, errorMessage: []};
                for (var i = 0; i < modelState[fieldName].length; i++) {
                    error.errorMessage.push(modelState[fieldName][i]);
                }
                errors.push(error);
            }
            return errors;
        }

        // Registers withErrors controller by form name (for internal use)
        function _register(formName, ctrl){
            withErrorCtrls[formName] = ctrl;
        }
    }
})(App);