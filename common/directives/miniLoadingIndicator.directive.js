var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("miniLoadingIndicator", function () {
		return {
			restrict: "E",
			replace: true,
			templateUrl: "/common/templates/miniLoadingIndicator.html"
		};
	});
}(App));