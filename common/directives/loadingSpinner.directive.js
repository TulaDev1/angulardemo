/* global Spinner: true
 */

var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("loadingSpinner", function () {
		return {
			restrict: "EA",
			replace: false,
			templateUrl: "/common/templates/loadingSpinner.html",
            link: function(scope, element, attrs) {
                var loadingContainer = element.find('[data-container="loader-container"]')[0];
                var spinner = new Spinner().spin();
                loadingContainer.appendChild(spinner.el);

                scope.$on("loader_show", function () {
                    return element.find('[data-container="loader-container"]').show();
                });
                return scope.$on("loader_hide", function () {
                    return element.find('[data-container="loader-container"]').hide();
                });
            }
		};
	});
}(App));