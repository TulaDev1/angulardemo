var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("headerLink", function () {
		return {
			restrict: "E",
			replace: true,
			controller: "HeaderLinkCtrl",
			controllerAs: "headerLinkCtrl",
			templateUrl: "/common/templates/headerLink.html",
			scope: {
				navText: "@text",
				navRoute: "@url"
			}
		};
	});
}(App));