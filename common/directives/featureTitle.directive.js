var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("featureTitle", function () {
		return {
			restrict: "E",
			replace: true,
			controller: "FeatureTitleCtrl",
			controllerAs: "featureTitleCtrl",
			templateUrl: "/common/templates/featureTitle.html",
			scope: {
				featureText: "@text",
				featureSubText: "@subtext",
				prevRoute: "@prevUrl",
				nextRoute: "@nextUrl"
			}
		};
	});
}(App));