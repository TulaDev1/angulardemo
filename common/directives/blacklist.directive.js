var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("blacklist", function () {
		return {
			require: "ngModel",
            priority:999999,
			link: function(scope, elem, attr, ngModel){
                var blacklist = ['bad word', 'very bad word'];

                //For DOM -> model validation
                ngModel.$parsers.unshift(function(value) {
                    var valid = blacklist.indexOf(value) === -1;
                    ngModel.$setValidity('blacklist', valid);
                    return value;
                });

                //For model -> DOM validation
                ngModel.$formatters.unshift(function(value) {
                    ngModel.$setValidity('blacklist', blacklist.indexOf(value) === -1);
                    return value;
                });
            }
		};
	});
}(App));