var App = App || {};

(function (app) {
	"use strict";

	app.common.directive("modal", function () {
		return {
			restrict: "E",
			replace: true,
			transclude: true,
			controller: "ModalCtrl",
			controllerAs: "modalCtrl",
			templateUrl: "/common/templates/modal.html",
			scope: {
				modalId: "@modalId",
				modalTitle: "@modalTitle",
				selfReference: "=reference"
			}
		};
	});
}(App));