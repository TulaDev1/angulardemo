var App = App || {};

(function (app) {
	"use strict";

	app.common = angular.module("common", ["ui.bootstrap", "ui.utils", "ngRoute", "ngAnimate", "toastr" ]);

	app.common.run(["$rootScope", "navigator", function ($rootScope, navigator) {
		navigator.bindScope($rootScope);
	}]);
}(App));


